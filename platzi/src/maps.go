package main

import "fmt"

//maps o diccionario de parking por ejemplos
func main() {
	m := make(map[string]int)

	m["Jose"] = 14
	m["Pepito"] = 20

	//map[Jose:14 Pepito:20]
	fmt.Println(m)

	// Recorrer map
	// OJO! no se crean en el mismo orden
	for i, v := range m {
		fmt.Println(i, v)
	}

	//sino existe, devuelve el zero value
	//poner el ok, nos dice si existe o no (bool)
	value, ok := m["JosePP"]

}
