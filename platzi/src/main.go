package main

import "fmt"
func main() {
	const pi float64 = 3.14
	const pi2 = 3.1415

	fmt.Println("hola mundo")
	
	fmt.Println("pi",pi)
	fmt.Println("pi2",pi2)

	//declaracion de variables enteres
	base := 12
	var altura int = 14
	var area int

	fmt.Println(base, altura, area);

	//Zero values
		//no tiene valor null
	var a int // default 0
	var b float64 // default 0
	var c string // defaulf ""
	var d bool // default false

	fmt.Println(a, b, c, d);

	//area cuadrado
	const baseCuadrado = 10
	areaCuadrado := baseCuadrado * baseCuadrado
	fmt.Println("Area cuadrado: ", areaCuadrado);

	//tipo de datos primitivos
	/*
	 aplican parecido con int pero este permite negativo
	 uint = depende del os (32 o 64 bits)
	 uint8 = 0 a 2^8 (0 a 255)
	 uint16 = 0 a 2^16-1
	 unit32 = 0 a 2^32-1
	 unit64 = 0 a 2^64-1

	 //Decimales
	 float32
	 float64

	 //textos y booleanos
	 string = ""
	 bool = true o false

	 //numeros completos (numero real e imaginario)
	 Complex64 = ambos con float32
	 Complex128 = ambos con float64
	 c := 10 + 8i
	*/
}