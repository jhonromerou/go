package main

import "fmt"

func main() {
	valor1 := 1
	valor2 := 2

	if valor1 == 1 {
		fmt.Println("Es 1!")
	} else {
		fmt.Println("No es 1")
	}

	//with and
	if valor1 == 1 && valor2 == 2 {
		fmt.Println("valor 1 y 2 se cumple")
	}

	//switch
	switch modulo := 4 % 2; modulo {
	case 0:
		// body
	default:
		//body
	}

	//switch sin condicion
	value := 50
	switch {
	case value > 100:
		//body
	case value < 0:
		//body
	default:
		//body
	}

	fmt.Println("keywords: defer, break, continue")

	//ejecutar la funcion al final
	//abrir archivo, se debe cerrar,
	//base de datos, debe cerrar
	defer fmt.Println("Hola")
	fmt.Println("Mundo")

}
