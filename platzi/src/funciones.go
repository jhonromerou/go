package main

import "fmt"

func normalFunction(message string) {
	fmt.Println(message)
}

func tripleArg(a, b int, c string) {
	fmt.Println(a, b, c)
}

func doubleReturn(a int) (c, d int) {
	return a, a * 2
}

func main() {
	normalFunction("Hola Mundo")

	//asignar double return
	value1, value2 := doubleReturn(3)
	//omite el value2
	//value1, _ := doubleReturn(3)
	fmt.Println("doble return: ", value1, value2)

	fmt.Println("Ciclos")
	for i := 1; i <= 10; i++ {
		fmt.Println("for condicional")
		break
	}

	//for while
	counter := 0
	for counter < 10 {
		fmt.Println("for while")
		counter++
		break
	}

	//for  forever
	//se ejecuta hasta que yo lo detenga
	counterForever := 0
	for {
		fmt.Println("for forevere")
		counterForever++
		break
	}

}
