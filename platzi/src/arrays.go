package main

import "fmt"

func main() {
	// Array
	//son inmutables, no se puede agregar
	var array [4]int
	fmt.Println(array)
	fmt.Println("Len dice la longuitud: ", len(array))
	fmt.Println("cap dice la capacitad: ", cap(array))

	// Slice
	fmt.Println("Slices: no tiene cantidad")

	slice := []int{0, 1, 2, 3, 4, 5, 6}
	fmt.Println("Len dice la longuitud: ", len(slice))
	fmt.Println("cap dice la capacitad: ", cap(slice))

	// MEtodos en el slice
	fmt.Println("Metodos")
	fmt.Println(slice[:3])  // hasta el [3] sin incluir
	fmt.Println(slice[2:4]) // el [2] y [3] no el 4
	fmt.Println(slice[4:])  // desde el [4] inclusivo

	// Append
	slice = append(slice, 7)
	fmt.Println("append: ", slice)

	// apend new list
	newSlice := []int{8, 9, 10}
	slice = append(slice, newSlice...)
	fmt.Println("append list: ", slice)

	fmt.Println("Recorrido slices con range")
	slice2 := []string{"hola", "que", "hace"}

	// i = indice, value
	for i, valor := range slice2 {
		fmt.Println(i, valor)
	}

}
