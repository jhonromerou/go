package main

import "fmt"

func main() {
	fmt.Println("en mefmt")

	// printf
	nombre := "Platzi"
	cursos := 500
	fmt.Printf("%s tiene más de %d cursos\n", nombre, cursos)

	// sprintf : Lo guarda como un string
	message := fmt.Sprintf("(2) %s tiene más de %d cursos", nombre, cursos)
	fmt.Println(message)

	//Tipo de datos
	fmt.Printf("tipo de dato de (nombre): %T\n", nombre)
}
