package main

import (
	"fmt"
	aliasPk "structs/src/mypackage"
)

type car struct {
	brand string
	year  int
}

type pc struct {
	ram   int
	disk  int
	brand string
}

func (myPc pc) ping() {
	fmt.Println(myPc.ram)
}

//Stringers
func (myPc pc) String() string {
	return fmt.Sprintf("Tengo %d GB RAM, %d GB disco y es una %s", myPc.ram, myPc.disk, myPc.brand)
}

func main() {
	myCar := car{brand: "Ford", year: 2020}
	fmt.Println(myCar)

	// Otra manera de instanciar
	// sino se deine usa el zero value que aplique
	var otherCar car
	otherCar.brand = "Ferrari"
	fmt.Println(otherCar)

	// MODIFICADORES DE ACCESO EN FUNCIONES
	// Y STRUCTS

	var myCard aliasPk.CarPublic
	myCard.Brand = "Kia Rio Space"
	myCard.Year = 2021
	fmt.Println(myCard)

	aliasPk.PrintMessage("Funcion publica (P)")

	// Structs y Punteros
	//punteros son acceso a la memoria,

	// a y b se vuelve 1 se actualizan siempre
	a := 50
	b := &a // guarda una direccion en memoria, no el valor

	fmt.Println(a, b)
	fmt.Println(a, *b) // el * accede al valor

	myPc := pc{ram: 12, disk: 100, brand: "Acer"}
	// Stringers --> func arriba
	fmt.Println(myPc)
	myPc.ping()

}
